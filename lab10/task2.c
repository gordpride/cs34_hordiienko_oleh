#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define PROGRAM_EXECUTION_TIME 5

void *producer_fun();
void *consumer_fun();

int global;
int flag = 0;

pthread_mutex_t mtx;
pthread_cond_t producer;
pthread_cond_t consumer;

int main(void) {
    pthread_t consumer_thread;
    pthread_t producer_thread;
    setbuf(stdout, NULL);

    global = 0;

    pthread_mutex_init(&mtx, NULL);
    pthread_cond_init(&producer, NULL);
    pthread_cond_init(&consumer, NULL);

    pthread_create(&consumer_thread, NULL, consumer_fun, NULL);
    pthread_create(&producer_thread, NULL, producer_fun, NULL);

    sleep(PROGRAM_EXECUTION_TIME);

    pthread_cancel(consumer_thread);
    pthread_cancel(producer_thread);

    pthread_mutex_destroy(&mtx);
    pthread_cond_destroy(&producer);
    pthread_cond_destroy(&consumer);

    exit(EXIT_SUCCESS);
}

void *producer_fun() {
    while(1) {
        sleep(1);
        while(flag == 1) {
            pthread_cond_wait(&producer, &mtx);
        }
        pthread_mutex_lock(&mtx);
        int minimum_number = 1;
        int max_number = 50;
        flag = 1;
        global = rand() % (max_number + 1 - minimum_number) + minimum_number;
        printf("\nЗроблено продукт: %d\n", global);
        pthread_cond_signal(&consumer);
        pthread_mutex_unlock(&mtx);
    }
}

void *consumer_fun() {
    while(1) {
        pthread_mutex_lock(&mtx);
        while(flag == 0) {
            pthread_cond_wait(&consumer, &mtx);
        }
        int value = global;
        global = 0;
        printf("\nОтримано продукт: %d\n", value);
        flag = 0;
        pthread_cond_signal(&producer);
        pthread_mutex_unlock(&mtx);
    }
}
