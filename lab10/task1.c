#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define SIZE 10
#define ITERATION 10

void *prodavec();
void *pokupec();
int produckt(int, int);
void item_b(int);
void AddBuff(int);
int RecBuff();

int array[SIZE];

sem_t empty_items;
sem_t full_items;
sem_t lock;

int main(void) {
    sem_init(&empty_items, 0, SIZE);
    sem_init(&lock, 0, 1);
    sem_init(&full_items, 0, 0);

    pthread_t consumer_thread;
    pthread_t producer_thread;

    pthread_create(&consumer_thread, NULL, pokupec, NULL);
    pthread_create(&producer_thread, NULL, prodavec, NULL);

    pthread_join(consumer_thread, NULL);
    pthread_join(producer_thread, NULL);

    sem_destroy(&empty_items);
    sem_destroy(&lock);
    sem_destroy(&full_items);

    exit(EXIT_SUCCESS);
}

void *prodavec() {
    int min = 1;
    int max = 50;
    for (int i = 0;i < ITERATION; i++) {
        int item = produckt(min, max); 
        printf("\nСтворено продукт: %d\n", item);
        printf("Очікування вільного місця\n");
        sem_wait(&empty_items);
        printf("Місце звільнилось\n");
        sem_wait(&lock); 
        AddBuff(item);
        sem_post(&lock); 
        sem_post(&full_items); 
    }
}

void *pokupec() {
    for (int i = 0; i < ITERATION; ++i) {
        printf("Очікування непорожнього буфера покупцем\n");
        sem_wait(&full_items);
        printf("Буфер не порожній\n");
        sem_wait(&lock);
        printf("Забираємо продукт\n");
        int item = RecBuff(); 
        sem_post(&lock);
        sem_post(&empty_items); 
        item_b(item);
    }
}

int produckt(int minimum_number, int max_number) {
    return rand() % (max_number + 1 - minimum_number) + minimum_number;
}

void item_b(int item) {
    fprintf(stderr, "Продукт %d витрачено покупцем\n", item);
}

void AddBuff(int num) {
    for (int i = 0; i < SIZE; i++) {
        if (array[i] == 0) {
            array[i] = num;
            break;
        }
    }
}

int RecBuff() {
    for (int i = 0; i < SIZE; i++) {
        if (array[i] != 0) {
            int save = array[i];
            array[i] = 0;
            return save;
        }
    }
}
