// libraries
#include<stdio.h>
#include<math.h>

// second function implementation with exponent, but g(x)
void function(double x) {
	double e = 2.718; // Euler number
	printf("\ng(x) = exp(-|x|)cos(x)	x = %0.3f\n", x); // condition
	printf("g(x) = %f \n", (double)(pow(e, (double)(-(fabs(x)))) * (double)cos(x))); // out of res
}

// main func
int main() {
	// call a function with the solution of an equation with an exponent, g(x).
	function((double)2.2);
	// end of program,
	return 0;
}

