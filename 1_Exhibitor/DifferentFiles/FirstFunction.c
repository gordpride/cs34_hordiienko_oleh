// libraries
#include<stdio.h>
#include<math.h>

// function implementation with exponent f(x)
void function(double x) {
	double e = 2.718; // euler number
	printf("\nf(x) = exp(-|x|)sin(x)	x = %0.3f\n", x); // condition of func
	printf("f(x) = %f \n", (double)(pow(e, (double)(-(fabs(x)))) * (double)sin(x))); // out
}

// main driver or main func
int main() {
	// call a function with the solution of an equation with an exponent, so f(x).
	function((double)2.2);
	// end of program,
	return 0;
}

