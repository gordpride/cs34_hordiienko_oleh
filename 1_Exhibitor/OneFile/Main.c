// libraries
#include<stdio.h>
#include<math.h>

// function implementation with exponent f(x)
void firstFunction(double x) {
	double e = 2.718; // Euler number
	printf("\nf(x) = exp(-|x|)sin(x)	x = %0.3f\n", x); // condition
	printf("f(x) = %f \n", (double)(pow(e, (double)(-(fabs(x)))) * (double)sin(x))); // out of result
}

// second function implementation with exponent, but g(x)
void secondFunction(double x) {
	double e = 2.718; // Euler number
	printf("\ng(x) = exp(-|x|)cos(x)	x = %0.3f\n", x); // condition
	printf("g(x) = %f \n", (double)(pow(e, (double)(-(fabs(x)))) * (double)cos(x))); // out of result
}

// main func
int main() {
	// call a function with the solution of an equation with an exponent, so f(x).
	firstFunction((double)2.2);
	// call a function with the solution of an equation with an exponent, but it is a g(x).
	secondFunction((double)2.2);
	// end of program,
	return 0;
}


