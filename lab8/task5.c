#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

#define STR_MAX_LEN 256

static pthread_key_t str_key;

void * fun1(void * args);

int main(int argc, char *argv[]){
	
    srand(time(NULL));

    int amount = atoi(argv[1]);
    pthread_t tid_arr [amount];

    static pthread_once_t once = PTHREAD_ONCE_INIT;

    for(int i = 0; i < amount; i++) {
        fprintf(stderr, "Потік %d запущено\n", i);
        if(pthread_create(tid_arr+i, NULL, &fun1, "текст, згенероване число") != 0) {
            fprintf(stderr, "Error!\n");
        }
    }

    for(int i = 0; i < amount; i++) {
        if (pthread_join(*(tid_arr+i), NULL) != 0) {
            fprintf(stderr, "Join error\n");
        }
        sleep(1);
    }

}

static void destructor(void * buf) {
    printf("Очищення буфера %p від потоку %u\n", buf, (unsigned int)pthread_self());
    free(buf);
    buf = NULL;
}

static void create_key(void) {
    int res;

    printf("TSD Ключ згенерований з потоку %u\n", (unsigned int)pthread_self());
    res = pthread_key_create(&str_key, destructor);
}

void * fun1(void * args) {
    char * str = (char *)args;

    int num = rand()%(5-1);

    for(int i = 0; i < num; i++) {
        int random = rand()%(10-1);
        printf("Thread %u, ", (unsigned int) pthread_self());

        static pthread_once_t once = PTHREAD_ONCE_INIT;
	    int res = pthread_once(&once, create_key);
	
	    char * buf = pthread_getspecific(str_key);
	    if (buf == NULL) { 
	        buf = (char*)malloc(STR_MAX_LEN);
	        printf("Створення буфера %p з черги %u\n", buf, (unsigned int)pthread_self());
	        res = pthread_setspecific(str_key, buf);
	
	        int i;
	        for (i = 0; str[i]; i++) {
	            buf[i] = str[i];
	        }
	        buf[i] = '\0';
	    }
	    printf("Рядок: %s %d\n", buf, random);    
    }
    return NULL;
}
