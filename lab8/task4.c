#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

double res;

void * fun1(void * args);

int main(int argc, char *argv[]) {

    pthread_t thread;
    int result = pthread_create(&thread, NULL, &fun1, NULL);

    sleep(atoi(argv[1]));
    printf("Завершення потоку %u\n", (unsigned int)thread);
    pthread_cancel(thread);

    void* iscanceled;

    if (pthread_join(thread, &iscanceled) != 0) {
        fprintf(stderr, "Join error\n");
        return 2;
    }

    if (iscanceled == PTHREAD_CANCELED)
        fprintf(stderr, "Потік скасовано\n");
    else {
        fprintf(stderr, "Потік завершено\n");
        fprintf(stderr, "Число пі: %.20f\n", res);
    }

}

void * fun1(void * args){
    int i = 0;

    int count = 100000, count1;
    double pi, a, b;

    int old_cancelled_state;
    for (count, count1 = 1; count != 0; count--, count1++) {
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &old_cancelled_state);
        if (count1 == 1) {
            a = 4 - 1;
            b = 1;
            pi = 4 / b - 4 / a;
            fprintf(stderr, "Доданок%d\t Pi %.20f\n", count1, pi);
        }
        if (count1 > 1) {
            a = a + 4;
            b = b + 4;
            pi = pi + (4 / b - 4 / a);
	    if(count1%10==0)
            	fprintf(stderr, "Доданок%d\t Pi %.20f\n", count1, pi);
        }
        pthread_setcancelstate(old_cancelled_state, NULL);
        pthread_testcancel();
    }
    res = pi;
}
