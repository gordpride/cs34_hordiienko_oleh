#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

void * fun1(void * args);

int main(int argc, char *argv[]) {

    int sec = atoi(argv[1]);

    pthread_t thread;
    int result = pthread_create(&thread, NULL, &fun1, NULL);

    sleep(sec);
    printf("Завершення потоку %u\n", (unsigned int)thread);
    pthread_cancel(thread);

    void* iscanceled;

    if (pthread_join(thread, &iscanceled) != 0) {
        fprintf(stderr, "Join error\n");
        return 2;
    }

    if (iscanceled == PTHREAD_CANCELED)
        fprintf(stderr, "Потік скасовано\n");
    else
        fprintf(stderr, "Потік завершено\n");

}

void final(void * arg) {
    int num = *((int*)arg);
    fprintf(stderr,"Завершення потоку нащадка %u. Отримане число %d\n",
           (unsigned int)pthread_self(), num);
}

void * fun1(void * args){
    int i = 0;
    int *arr = &i;

    pthread_cleanup_push(final, arr);

    if(pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL) != 0) {
        fprintf(stderr, "Неможливо змінити тип\n");
        exit(EXIT_FAILURE);
    }

    while(1) {
        fprintf(stderr, "Ітерація %d\n", ++i);
        sleep(1);
    }
    pthread_cleanup_pop(1);
}
