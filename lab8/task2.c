#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

void * threadFunc(void * args);

int main(int argc, char *argv[]) {

    pthread_t thread;
    int result = pthread_create(&thread, NULL, &threadFunc, NULL);

    sleep(atoi(argv[1]));
    printf("Початок завершення потоку %u\n", (unsigned int)thread);
    pthread_cancel(thread);

    void* iscanceled;

    if (pthread_join(thread, &iscanceled) != 0) {
        fprintf(stderr, "Join error\n");
        return 2;
    }

    if (iscanceled == PTHREAD_CANCELED)
        fprintf(stderr, "Потік скаcовано\n");
    else
        fprintf(stderr, "Потік завершено\n");

}

void * threadFunc(void * args){
    int i = 0;

    if(pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL) != 0) {
        fprintf(stderr, "Неможливо змінити тип\n");
        exit(EXIT_FAILURE);
    }

    while(1) {
        fprintf(stderr, "Ітерація: %d\n", ++i);
        sleep(1);
    }
}
