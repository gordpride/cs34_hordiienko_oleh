#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

struct thread_arg {
    int num;
};

void * threadFunc(void * args);

int main(int argc, char *argv[]) {

    pthread_t thread;
    struct thread_arg targ;
    targ.num = atoi(argv[1])*2;
    int result = pthread_create(&thread, NULL, &threadFunc, &targ);

    sleep(atoi(argv[1]));
    printf("Зупинка потоку %u\n", (unsigned int)thread);
    pthread_cancel(thread);

    void* iscanceled;

    if (pthread_join(thread, &iscanceled) != 0) {
        fprintf(stderr, "Join error\n");
        return 2;
    }

    if (iscanceled == PTHREAD_CANCELED)
        fprintf(stderr, "Потік скасовано\n");
    else
        fprintf(stderr, "Потік завершено\n");

}

void * threadFunc(void * args){
    struct thread_arg targ = *(struct thread_arg *) args;

    if(pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL) != 0) {
        fprintf(stderr, "Неможливо змінити стан\n");
        exit(EXIT_FAILURE);
    }

    for(int i = 0; i < targ.num; i++) {
        fprintf(stderr, "Ітерація %d\n", i+1);
        sleep(1);
    }
}
