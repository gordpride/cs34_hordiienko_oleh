#include<stdio.h>
#include<stdlib.h>
#include<math.h>

// func, which use scanf to take a value, after this value will be used to define an range of random or size of array, etc.
int inputValue() {
	int value;
	printf("\nEnter a value: ");
	scanf("%d", &value);
	return value;
}

// print array
void printArray(int array[], int sizeOfArray) {
	printf("\nArray\n");
	for(int i = 0; i < sizeOfArray; i++) {
		printf("%d ", array[i]);
	}
}

// func find a middle value (mean) in an array
double findMean(int array[], int sizeOfArray) {
	double mean = 0;
	for(int i = 0; i < sizeOfArray; i++) {
		mean += array[i];
	}
	return mean/sizeOfArray;
}

// func find an amount of elements more than middle value (mean)
int findElemMoreMean(int array[], int sizeOfArray) {
	double mean = findMean(array, sizeOfArray); // firstly, find a mean
	printf("\nMean = %.2f", mean);
	int counterOfElemMoreMean = 0; // initiazation of counter will find an elem more than mean
	
	for(int i = 0; i < sizeOfArray; i++) {
		if(array[i] > mean) {
			counterOfElemMoreMean++;
		}
	}
	return counterOfElemMoreMean;
}

// func find sum of module elements after first negative elem 
int sumAfterNegativeElem(int array[], int sizeOfArray) {
	int sumOfElems = 0; // initialization of sum will be increase, if array has a negative elem, else return 0
	for(int i = 0; i < sizeOfArray; i++) {
		// condition, where sum will increase
		if(array[i] < 0) { 
			// why j = i + 1? because sum will increase AFTER negative elem, if leave a j = i, first negative number will be added to sum
			for(int j = i + 1; j < sizeOfArray; j++) {
				sumOfElems += abs(array[j]);
			}
			i = sizeOfArray; // simple way to stop a work of cycle for :)
		}
	}
	return sumOfElems;
}

// main func
int main() {
	printf("\nSize of array ->");
	int sizeOfArray = inputValue(); // initialization size of array
	
	int array[sizeOfArray]; // initialization array
	printArray(array, sizeOfArray); // print array
	
	printf("\nDefine range of random filling array. \nFrom ->");
	
	int fromRange = inputValue(); // defining a range of random, firstly - a start value
	printf("\nTo -> ");
	
	int toRange = inputValue(); // secondly - an end value
	for(int i = 0; i < sizeOfArray; i++) {
		array[i] = (rand() % (toRange - fromRange + 1)) + fromRange; // filling an array to rand values
	}
	
	printArray(array, sizeOfArray); // print array
	
	printf("\nAmount of elements more than mean = %d", findElemMoreMean(array, sizeOfArray)); 
	printf("\nSum of elements after first negative = %d", sumAfterNegativeElem(array, sizeOfArray));
	
	return 0;
}
