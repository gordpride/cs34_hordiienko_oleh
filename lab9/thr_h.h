#ifndef THR_H_
#define THR_H_

typedef struct arg_str {
   int n;
   double a, b, eps; 
   double (*f)(double);
} TARG;

double threadIntegral(const TARG * arg);

#endif
