#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "thr_h.h"
#include "thr_c.c"
#include "int_h.h"
#include "int_c.c"

double fun(double x) {
    return 4.0 - x*x;
}

int main(int argc, char ** argv) {
    int thread_amount = 5;
    int opt;
    while ((opt = getopt(argc, argv, "1:")) != -1) {
        switch (opt) {
            case '1':
                thread_amount = atoi(optarg);
                break;
        }
    }

    double res;
    printf("Обчислення інтегралу\n");
    printf("Запуск потоку\n");
    {
        TARG arg = {thread_amount, 0.0, 2.0, 1.0e-6, fun};
        res = threadIntegral(&arg);
    }
    printf("Результат: %g\n", res);

    return EXIT_SUCCESS;
}
