#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <getopt.h>

void * increment(void * arg);
void * decrement(void * arg);

static long global = 0;

int main(int argc, char ** argv) {
    long num = 300000;
    int am = 2;

    setbuf(stdout, NULL);
    int opt;
    while ((opt = getopt(argc, argv, "1:2:")) != -1) {
        switch (opt) {
            case '1':
                am = atoi(optarg);
                break;
            case '2':
                num = atoi(optarg);
                break;
        }
    }

    printf("Робота з %ld ітерація в %d парі з потоком\n", num, am);
    printf("Ініціалізація глобальної змінної: %ld\n", global);

    int res;
    pthread_t * tid1, * tid2;
    tid1 = (pthread_t*)calloc(am, sizeof(pthread_t));
    tid2 = (pthread_t*)calloc(am, sizeof(pthread_t));

    for(int i = 0; i < am; i++) {
        if (res = pthread_create(tid1+i, NULL, increment, &num)) {
            fprintf(stderr, "Створення інкременту ERROR (%d)\n", res);
            exit(1);
        }
        if (res = pthread_create(tid2+i, NULL, decrement, &num)) {
            fprintf(stderr, "Створення декрименту ERROR (%d)\n", res);
            exit(1);
        }
    }

    for(int i = 0; i < am; i++) {
        if (pthread_join(*(tid1 + i), NULL)) {
            fprintf(stderr, "JOIN ERROR\n");
            exit(2);
        }
        if (pthread_join(*(tid2 + i), NULL)) {
            fprintf(stderr, "JOIN ERROR\n");
            exit(2);
        }
    }

    printf("Глобальна змінна: %ld\n", global);

    return 0;
}

void * increment(void * arg) {
    long num = *(long*)arg;
    long i, local;
    for (i = 0; i < num; i++) {
        local = global;
        local++;
        global = local;
    }
    return NULL;
}

void * decrement(void * arg) {
    long num = *(long*)arg;
    long i, local;
    for (i = 0; i < num; i++) {
        local = global;
        local--;
        global = local;
    }
    return NULL;
}
