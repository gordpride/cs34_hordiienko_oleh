#include <stdio.h>  
#include <conio.h>  
#include <stdlib.h>  

int toGenerateRandomNum() {
	return rand() % 7 + 0;
}

int toStartGame() {
	return 5000;
}

int iAmLucky(int firstValue, int secondValue, int thirdValue) {
	if(firstValue == 0 && secondValue == 0 && thirdValue == 0) {
		printf("Lucky zero! Combinations - 0 0 0 \n+600 coins!\n");
		return 600;
	} else if (firstValue == 1 && secondValue == 1 && thirdValue == 1) {
		printf("Triple 1! Combinations - 1 1 1 \n+100 coins!\n");
		return 100;
	} else if (firstValue == 2 && secondValue == 2 && thirdValue == 2) {
		printf("Triple 2! Combinations - 2 2 2 \n+200 coins!\n");
		return 200;
	} else if (firstValue == 3 && secondValue == 3 && thirdValue == 3) {
		printf("Triple 3! Combinations - 3 3 3 \n+300 coins!\n");
		return 300;
	} else if (firstValue == 4 && secondValue == 4 && thirdValue == 4) {
		printf("Triple 4! Combinations - 4 4 4 \n+440 coins!\n");
		return 440;
	} else if (firstValue == 5) {
		if(secondValue == 5) {
			if(thirdValue == 5) {
				printf("Triple 5! OMG! Combinations - 5 5 5 \n+555 coins!\n");
				return 555;
			} else {
				printf("Double 5! Not bad. Combinations - 5 5 X \n+550 + Third Number coins!\n");
				return 550 + thirdValue;
			}
		} else {
			printf("One 5! Ok. Combinations - 5 X X \n+500 + Second + Double Numbers coins!\n");
			return (5 * 10 + secondValue) * 10 + thirdValue;
		}
	} else if(firstValue == 6 && secondValue == 6 && thirdValue == 6) {
		printf("Oops!Thinks look blue.. Combinations - 6 6 6 \n-666 coins! :(\n");
		return -666;
	} else if (firstValue == 7) {
		if(secondValue == 7) {
			if(thirdValue == 7) {
				printf("JACKPOOOOOOOOOT!!! ARE YOU CRAZY?\n");
				printf("YOU WIN!! 1 000 000 coins!\n");
				return -2000000;
			} else {
				printf("Double 7! You were close. Combinations - 7 7 X \n+770 + Third Number coins!\n");
				return 770 + thirdValue;
			}
		} else {
			printf("One 7! Ohh.. Combinations - 7 X X \n+700 + Second + Double Numbers coins!\n");
			return (7 * 10 + secondValue) * 10 + thirdValue;
		}
	} else {
		printf("No this time dude..\n");
		printf("Your combinations: %d %d %d\n", firstValue, secondValue, thirdValue);
		return 0;
	}
}

int main(){
    int balance = toStartGame();
	printf("Are you ready?\n");
	system("pause");
	printf("\n");
	
	while(balance > 0) {
	balance += iAmLucky(toGenerateRandomNum(), toGenerateRandomNum(), toGenerateRandomNum()) - 100;
	printf("\nYour balance = %d", balance);
	printf("\nI am lucky! Press Enter.\n");
	system("pause");
	printf("\n");
	}
	
	return 0;   
}  
