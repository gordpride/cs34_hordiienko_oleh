#include <stdio.h>
#include <stdlib.h>

#define MIN 	2100000

int n;

void initArray(int **arr) {
	*arr = calloc(n, sizeof(int));  // Creating enough space for 'n' integers.
    if (arr == NULL) {
        printf("Unable to allocate memory\n");
    }
}

void decreaseArray(int **arr) {
	*arr = realloc(*arr, (n - 1) * sizeof(int));
	n--;
}

void increaseArray(int **arr) {
	*arr = realloc(*arr, (n + 1) * sizeof(int));
	n++;
}

void add(int **arrP, int *arr, int value) {
	increaseArray(arrP);
	arr[n - 1] = value;
}

void insert(int **arrP, int *arr, int value, int index) {
	increaseArray(arrP);
	for(int i = n - 1; i > index; i--) {
		arr[i] = arr[i-1];
	}
	arr[index] = value;
}

void size() {
	printf("Size of array = %d\n", n);
}

void removeElem(int **arrP, int *arr, int index) {
	for(int i = index; i < n; i++) {
		arr[i] = arr[i+1];
	}
	decreaseArray(arrP);
}

void set(int *arr, int value, int index) {
	arr[index] = value;
}

void getElem(int *arr, int index) {
	printf("Elem with index %d: %d\n", index, arr[index]);
}

void print(int *arr) {
	printf("Given Array: ");
    for (int i = 0; i < n; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

int main() {
    printf("Size of the array: ");
    scanf("%d", &n);
    int *arr;  // Creating enough space for 'n' integers.
    
    initArray(&arr);
    
    for (int i = 0; i < n; i++)
        arr[i] = (rand() % (10 - (-10) + 1)) + -10;  // Notice that, (arr + i) points to ith element

    printf("Given array: ");
    for (int i = 0; i < n; i++)
        printf("%d ", *(arr + i));  // Dereferencing the pointer
    printf("\n");
	
	add(&arr, arr, 4);
	print(arr);

	insert(&arr, arr, 1000, 3);
	print(arr);
	
	size();
	
	removeElem(&arr, arr, 3);
	print(arr);
	
	set(arr, 993, 3);
	print(arr);
	
	getElem(arr, 3);
	
    free(arr);
    return 0;
}
